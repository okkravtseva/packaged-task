#include <iostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <future>
#include <functional>
#include <queue>

using namespace std;

template<typename T>
class threadsafe_queue
{
private:
    mutable std::mutex mut;
    std::queue<T> data_queue;
    std::condition_variable data_cond;
public:
    threadsafe_queue()
    {}
    threadsafe_queue(threadsafe_queue const& other)
    {
        std::lock_guard<std::mutex> lk(other.mut);
        data_queue = other.data_queue;
    }

    void push(T new_value)
    {
        std::lock_guard<std::mutex> lk(mut);
        data_queue.push(new_value);
        data_cond.notify_one();
    }

    void wait_and_pop(T& value)
    {
        std::unique_lock<std::mutex> lk(mut);
        data_cond.wait(lk, [this] {return !data_queue.empty(); });
        value = data_queue.front();
        data_queue.pop();
    }
    bool empty() const
    {
        std::lock_guard<std::mutex> lk(mut);
        return data_queue.empty();
    }

};

using ullong = unsigned long long;
using tqueue = threadsafe_queue<int>;

ullong calc_factorial(int n)
{
    ullong res = 1;
    for (int i = 2; i <= n; ++i)
        res *= i;
    return res;
}
mutex m;
condition_variable cv;
tqueue tsqueue;
bool ready = false;
bool processed = false;

int main()
{
    packaged_task<bool(void)> task_factorial([]() {
// Wait until main() puts data
        while (true)
        {
            unique_lock<mutex> lk(m);
            cv.wait(lk, [] {return ready; });
            if (tsqueue.empty())
                break;
            int n;
            tsqueue.wait_and_pop(n);
            cout << "Working thread: get number " << n << " to calculate" << endl;
            ullong res = calc_factorial(n);
            cout << "Working thread: factorial(" << n << ") = " << res << endl;
            processed = true;
            ready = false;
            cv.notify_one();
        }
        return true;
        });
    future<bool> res = task_factorial.get_future();
    thread thread_factorial(move(task_factorial));
    // Repeat 10 times:
    for (int i = 1; i <= 10; i++)
    {
        cout << "Main thread: pushing number " << i << " to calculate" << endl;
        tsqueue.push(i);
        processed = false;
        ready = true;
        cv.notify_one();
        unique_lock<mutex> lk(m);
        cv.wait(lk, [] {return processed; });
    }
    ready = true;
    cv.notify_one();
    thread_factorial.join();
    cout << "Working thread finished with code " << boolalpha << res.get() << endl;
/*
    threadsafe_queue<packaged_task<ullong(int)>> tsgueue;

    packaged_task<ullong(int)> task([](int n) {
        ullong res = 1;
        for (int i = 2; i <= n; ++i)
            res *= i;
        return res;
        });
    tsgueue.push(move(task));
    thread thread_factorial();
    for(int i=1; i <= 10; i++)
    {
        packaged_task<ullong(int)> task([](int n) {
            ullong res = 1;
        for (int i = 2; i <= n; ++i)
            res *= i;
        return res;
        });

        thread_factorial(move(task), 2, 10);
    task_td.join();
    std::cout << "2^10 = " << result.get() << '\n'
*/

    return 0;
}
